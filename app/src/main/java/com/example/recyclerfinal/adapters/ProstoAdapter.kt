package com.example.recyclerfinal.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerfinal.MyMusic
import com.example.recyclerfinal.R
import com.example.recyclerfinal.music


class ProstoAdapter(val loccontext: Context, val spisok: ArrayList<music>): RecyclerView.Adapter<ProstoAdapter.VH>( ){
    class VH(hz: View): RecyclerView.ViewHolder(hz) {
      val text1: TextView = hz.findViewById(R.id.artist)
      val text2: TextView = hz.findViewById(R.id.song)
      val pict: ImageView = hz.findViewById(R.id.picture)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProstoAdapter.VH {
      val loot = LayoutInflater.from(loccontext).inflate(R.layout.adapter, parent, false)
        return VH(loot)
    }

    override fun onBindViewHolder(holder: ProstoAdapter.VH, position: Int) {
        holder.text1.setText(spisok[position].artist)
        holder.text2.setText(spisok[position].song)
        holder.pict.setImageResource(spisok[position].image)
    }

    override fun getItemCount(): Int {
       return spisok.size
    }
}